package assigment

import cats.effect.IO
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import sttp.client._


class Finder(console: Console[IO]) {

  case class Repository(name: String, stargazers_count: Int)

  case class Subscriber(login: String)

  case class Answer(owner: String, repository: Repository)

  implicit val backend: SttpBackend[Identity, Nothing, NothingT] = HttpURLConnectionBackend()

  def createStringWithRepos(topRepos: Seq[String]): String = {
    val concatRepos: (String, String) => String = (s1, s2) => s1 + s2 + "\n"
    s"${topRepos.fold("")(concatRepos)}"
  }


  def getRepositories(nickName: String, maxStar: Int): String = {
    val request: Request[Either[String, String], Nothing] = basicRequest.get(
      uri"https://api.github.com/users/$nickName/repos")
    val repositories: Seq[Repository] = request.send().body.map(decode[List[Repository]]).toOption.get.toOption.get
    val topRepositories = for (r <- repositories if (r.stargazers_count > maxStar)) yield s"https://github.com/$nickName/${r.name}"
    createStringWithRepos(topRepositories)
  }

  def getTopUserRepo(nickName: String): Int = {
    val request: Request[Either[String, String], Nothing] = basicRequest.get(
      uri"https://api.github.com/users/$nickName/repos")
    val repositories: Seq[Repository] = request.send().body.map(decode[List[Repository]]).toOption.get.toOption.get
    Some(repositories).map(_.maxBy(_.stargazers_count)).get.stargazers_count
  }

  def getSubscribers(nickName: String): List[Subscriber]
  = {
    val req1 = basicRequest.get(uri"https://api.github.com/users/$nickName/followers")
    req1.send().body.map(decode[List[Subscriber]]).toOption.get.toOption.get
  }

  def start(): IO[Unit] = for {
    _ <- console.printLine("Введите никнейм: ")
    nickName <- console.readLine
    currentUserTopRepository = getTopUserRepo(nickName)
    subscribers = getSubscribers(nickName)
    topRepositories: Seq[String] = for (subscriber <- subscribers) yield getRepositories(subscriber.login, currentUserTopRepository)
    repos = createStringWithRepos(topRepositories)
    _ <- console.printLine(repos)
  } yield ()
}



