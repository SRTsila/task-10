package assigment

import cats.effect.{ExitCode, IO, IOApp}

import scala.io.StdIn

object Main extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val console = new Console[IO] {
      override def printLine(text: String): IO[Unit] = IO(println(text))

      override def readLine: IO[String] = IO(StdIn.readLine())
    }
    new Finder(console).start().map(_ => ExitCode.Success)
  }
}
